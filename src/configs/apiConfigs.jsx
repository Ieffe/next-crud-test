import axios from "axios";
import { notFound, redirect } from "next/navigation";

export const EndPoint =
  "https://crudcrud.com/api/a9a8f324c6ed4b499c09d15100971fcc"; //mess up the url for error testing
// 'https://crudcrud.com/api/f849390ab4e14fc887d639ab6f64dd45' for testing 400 (expired endpoint)

export async function Request({ url, method, payload, withAuth = false }) {
  axios.interceptors.request.use(
    (request) => {
      console.log(request.status);
      console.log("Request moving ...");
      return request;
    },
    (error) => {
        return error
    }
  );

  axios.interceptors.response.use(
    (response) => {
      console.log("Response moving ...");
      console.log(response.status);
      return response;
    },
    (error) => {
      console.log("Response Error!");
      // notFound() // return a 404 is working
      // redirect('/test') //redirect also working
      console.error(error.response);
      switch (error.status) {
        case 404:
          console.log(error.status);
          notFound();
        case 401:
          console.log(error.status);
          //return to the login page if exist
        default:
          console.log(error.status);
          return error
      }
    }
  );

  return axios({
    baseURL: EndPoint,
    url: url,
    method: method,
    data: payload,
    headers: withAuth
      ? {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        }
      : {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
  });
}
