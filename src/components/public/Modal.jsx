'use client'
export function Modal({children, close}){
    return(
        <div onClick={close} className="flex flex-row w-full h-full justify-center items-center fixed z-10 bg-black/50 top-0 left-0">
            <div onClick={(e)=>(e.stopPropagation())} className="w-1/3 border-white border bg-black p-4 rounded-lg">
                {children}
            </div>
        </div>
    )
}