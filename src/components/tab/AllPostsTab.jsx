import { getPostsSWR } from "@/services/postService";
import { DeleteModal } from "../posts/DeleteModal";
import { EditModal } from "../posts/EditModal";

export default function AllPosts() {
  const { data, error, mutate } = getPostsSWR();

  if (error) {
    return <p>Error: {error.message}</p>;
  }

  if (!data) {
    return <p>Loading...</p>;
  }

  console.log(data);
  const datas = data.data;
  // console.log(datas);
  return (
    <div className="mt-4 mx-3">
      {datas.length > 0 ? (
        datas.map((d) => (
          <div className="p-3 mb-3 border-white  border rounded-lg" key={d._id}>
            <p>{d.title}</p>
            <p>{d.content}</p>
            <div className="flex flex-row gap-x-2 mt-3">
              <EditModal {...d}  />
              <DeleteModal {...d} />
            </div>
          </div>
        ))
      ) : (
        <p>There is no Data, please add one!</p>
      )}
    </div>
  );
}
