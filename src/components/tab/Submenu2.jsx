import Tabs from "@/configs/tabConfig";
import { FormManagerContext } from "@/context/FormContext";
import { useContext, useEffect, useRef, useState } from "react";

export default function Nested2({ tabsList }) {
  //   const [tabs, setTabs] = useState([]);
  //   const [activeTab, setActiveTab] = useState(null);
  //   const [showSideMenu, setShowSideMenu] = useState(false);

  const {
    tabs,
    setTabs,
    activeN2Tab,
    setActiveN2Tab,
    showSideMenu,
    setShowSideMenu,
  } = useContext(FormManagerContext);

  const sideMenuRef = useRef();

  useEffect(() => {
    console.log(showSideMenu);
  }, [showSideMenu]);

  useEffect(() => {
    const handleClickOutside = (event) => {
      if (sideMenuRef.current && !sideMenuRef.current.contains(event.target)) {
        setShowSideMenu(false);
      }
    };

    document.addEventListener("click", handleClickOutside);

    return () => {
      document.removeEventListener("click", handleClickOutside);
    };
  }, [sideMenuRef]);

  const switchTab = (id) => {
    const selectedTab = tabs.find((tab) => tab.id === id);
    setActiveN2Tab(selectedTab ? selectedTab.component : null);
  };

  const addTab = (id, component) => {
    const existingTab = tabs.find((tab) => tab.id === id);

    if (existingTab) {
      setActiveN2Tab(existingTab.component);
    } else {
      setTabs((prevTabs) => [...prevTabs, { component, id }]);
      setActiveN2Tab(component);
    }

    setShowSideMenu(false);
  };

  const closeTab = (component, index) => {
    const newTabs = tabs.filter((_, id) => id !== index);
    setTabs(newTabs);

    if (activeN2Tab === component) {
      if (index > 0) {
        setActiveN2Tab(newTabs[index - 1].component);
      } else if (newTabs.length > 0) {
        setActiveN2Tab(newTabs[0].component);
      } else {
        setActiveN2Tab(null);
      }
    }
  };

  const renderActiveComponent = () => {
    return activeN2Tab || null;
  };

  return (
    <div className="flex flex-col  h-full">
      {tabs.length > 0 ? (
        <>
          <div className="flex flex-row">
            {tabs.map((tab, index) => (
              <div
                key={index}
                className={`flex flex-shrink-0  flex-row gap-x-3 justify-between px-3 py-2 border-white rounded-t-lg ${
                  tab.component === activeN2Tab ? "border-x border-t" : " border"
                }`}
                onClick={() => switchTab(tab.id)}
              >
                <span>
                  {" "}
                  {Tabs.find((t) => t.id === tab.id)?.name ||
                    Tabs.find((t) =>
                      t.submenus?.some((s) => s.id === tab.id)
                    )?.submenus.find((s) => s.id === tab.id)?.name}
                </span>
                <span
                  onClick={() => {
                    closeTab(tab.component, tab.id);
                  }}
                >
                  ×
                </span>
              </div>
            ))}
            <div className="relative flex flex-row" ref={sideMenuRef}>
              <div
                className={`flex flex-shrink-0  flex-row gap-x-3 justify-between px-3 py-2 border-white rounded-t-lg border`}
                onClick={() => {
                  setShowSideMenu(!showSideMenu);
                }}
              >
                <span>+</span>
              </div>
              {showSideMenu ? (
                <div id="sideMenu" className="absolute left-[100%] min-w-max">
                  <div className="grid grid-cols-3 bg-black border border-white">
                    {tabsList.map((s, index) => (
                      <div
                        key={index}
                        className="flex flex-col items-center p-5"
                        onClick={() => addTab(s.id, s.component)}
                      >
                        <div className="">{s.icon}</div>
                        <span className="mt-3">{s.name}</span>
                      </div>
                    ))}
                  </div>
                </div>
              ) : (
                ""
              )}
            </div>
            <div className="w-full border-b" />
          </div>
          <div className="content h-full border-l p-6">
            {renderActiveComponent()}
          </div>
        </>
      ) : (
        <div>
          <h1 className="text-2xl font-bold text-center mb-2">Select menu!</h1>
          <div className="grid grid-cols-3 bg-black border border-white">
            {tabsList.map((t, index) => (
              <div
                onClick={() => addTab(t.id, t.component)}
                className="w-full flex flex-col items-center p-6"
                key={index}
              >
                <div>{t.icon}</div>
                <p>{t.name}</p>
              </div>
            ))}
          </div>
        </div>
      )}
    </div>
    //reminder: make function that setActive as the basecomponent first
    //then
  );
}
