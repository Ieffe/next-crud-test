import { FormManagerContext } from "@/context/FormContext";
import { create } from "@/services/form2Service";
import { useContext, useState } from "react";

const Step1 = ({ data, handleChange, nextStep }) => {
  return (
    <div>
      <h2>Step 1</h2>
      <input
        type="text"
        name="a"
        value={data.a}
        onChange={handleChange}
      />
      <button onClick={nextStep}>Next</button>
    </div>
  );
};

const Step2 = ({ data, handleChange, prevStep, nextStep }) => {
  return (
    <div>
      <h2>Step 2</h2>
      <input
        type="text"
        name="b"
        value={data.b}
        onChange={handleChange}
      />
      <button onClick={prevStep}>Prev</button>
      <button onClick={nextStep}>Next</button>
    </div>
  );
};

const Step3 = ({ data, handleChange, prevStep, submit }) => {

  return (
    <div>
      <h2>Step 3</h2>
      <input
        type="text"
        name="c"
        value={data.c}
        onChange={handleChange}
      />
      <button onClick={prevStep}>Prev</button>
      <button onClick={submit}>Submit</button>
    </div>
  );
};

export default function StepForm() {

  const { form2State, updateForm2State, step, setStep } = useContext(FormManagerContext)

 


  const handleChange = (e) => {
    const { name, value } = e.target;
    updateForm2State({ ...form2State, [name]: value });
  };

  const nextStep = () => {
    if (step < 3) {
      setStep(step + 1);
    }
  };

  const prevStep = () => {
    console.log("Going back to the previous step");
    if (step > 1) {
      setStep(step - 1);
    }
  };

  const  submitHandler = async (e) => {
    e.preventDefault()
    const payload = {
      a: form2State.a,
      b: form2State.b,
      c: form2State.c
    }
    await create(payload)
  }

  return (
    <div>
      {step === 1 && (
        <Step1 data={form2State} handleChange={handleChange} nextStep={nextStep} />
      )}
      {step === 2 && (
        <Step2 data={form2State} handleChange={handleChange} prevStep={prevStep} nextStep={nextStep}  />
      )}
      {step === 3 && (
        <Step3 data={form2State} handleChange={handleChange} prevStep={prevStep} submit={submitHandler}/>
      )}
    </div>
  );
}