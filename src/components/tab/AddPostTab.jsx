import { FormManagerContext } from "@/context/FormContext";

const { addPosts } = require("@/services/postService");
const { useState, useEffect, useContext } = require("react");

export default function AddPost() {
  const { formState, updateFormState } = useContext(FormManagerContext);

  // const [form, setForm] = useState({
  //   title: "",
  //   content: "",
  // });

  const [valid, setValid] = useState({
    title: false,
    content: false,
  });
  const [submitted, setSubmitted] = useState(false);

  const validate = () => {
    const newValid = {
      title: !!formState.title,
      content: !!formState.content,
    };
    setValid(newValid);
  };

  useEffect(() => {
    if (valid.title && valid.content) {
      handleSubmit();
    }
  }, [valid.title, valid.content]);

  const handleSubmit = async () => {
    const payload = {
      title: formState.title,
      content: formState.content,
    };

    await addPosts(payload);
    updateFormState({
      title: "",
      content: "",
    });
  };

  return (
    <div className="p-4">
      <form
        onSubmit={(e) => {
          e.preventDefault();
          validate();
          // No changes to the input elements, they still use formState
          // to display and update values.
          setSubmitted(true);
        }}
      >
        {!valid.title && submitted && <p>Title must not be empty</p>}
        <input
          onChange={(e) => updateFormState({ title: e.target.value })}
          type="text"
          placeholder="Insert Title"
          value={formState.title}
        />
        {!valid.content && submitted && <p>Content must not be empty</p>}
        <textarea
          onChange={(e) => updateFormState({ content: e.target.value })}
          name=""
          placeholder="Add Some Text"
          id=""
          cols="30"
          rows="10"
          value={formState.content}
        ></textarea>
        <button type="submit">Submit</button>
      </form>
    </div>
  );
}
