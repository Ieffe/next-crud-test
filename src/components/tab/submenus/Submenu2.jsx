import { FormManagerContext } from "@/context/FormContext";
import { useContext, useEffect, useState } from "react";

export default function Submenu1() {
  const { formDirtyData, setFormDirtyData, isDirty, setIsDirty } =
    useContext(FormManagerContext);

  

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormDirtyData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
    setIsDirty(true);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    // Add logic to save form data
    setIsDirty(false);
    alert("Form data submitted!");
  };

  return (
    <div>
      <h1>Dirty Form Example</h1>
      <form onSubmit={handleSubmit}>
        <label>
          Name:
          <input
            type="text"
            name="name"
            value={formDirtyData.name}
            onChange={handleChange}
          />
        </label>
        <br />
        <label>
          Email:
          <input
            type="text"
            name="email"
            value={formDirtyData.email}
            onChange={handleChange}
          />
        </label>
        <br />
        <button type="submit">Submit</button>
      </form>
    </div>
  );
}
