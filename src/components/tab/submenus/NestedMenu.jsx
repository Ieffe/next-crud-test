import { Modal } from "@/components/public/Modal";
import { FormManagerContext } from "@/context/FormContext";
import { useContext, useState } from "react";

const Nested1 = () => {
  const [showModal, setShowModal] = useState(false);

  const openModal = () => {
    setShowModal(true);
  };
  const closeModal = () => {
    setShowModal(false);
  };

  return (
    <div>
      <p>Nested Tab 1</p>
      <button onClick={openModal}>Open Modal</button>
      {showModal ? (
        <Modal close={closeModal}>
          <div className="flex flex-col">
            <div className="self-end">
              <button className="button-red" onClick={closeModal}>
                X
              </button>
            </div>
            <div>
              <p>Test Some Modal Here</p>
            </div>
          </div>
        </Modal>
      ) : (
        ""
      )}
    </div>
  );
};

const Nested2 = () => {
  return (
    <div>
      <p>Nested Tab 2</p>
    </div>
  );
};

const Nested3 = () => {
  return (
    <div>
      <p>Nested Tab 3</p>
    </div>
  );
};

export default function NestedMenu() {
  const sideTabs = [
    {
      name: "1",
      component: <Nested1 />,
    },
    {
      name: "2",
      component: <Nested2 />,
    },
    {
      name: "3",
      component: <Nested3 />,
    },
  ];

  //   const [activeTab, setActiveTab] = useState(null);
  const { activeTab, setActiveTab } = useContext(FormManagerContext);

  const renderCurrentTab = () => {
    if (activeTab != null) {
      return activeTab;
    } else {
      return <p>Null.</p>;
    }
  };

  return (
    <div className="flex flex-row">
      <div className="flex flex-col">
        {sideTabs.map((s, index) => (
          <button onClick={() => setActiveTab(s.component)} key={index}>
            {s.name}
          </button>
        ))}
      </div>
      <div className="flex flex-col">{renderCurrentTab()}</div>
    </div>
  );
}
