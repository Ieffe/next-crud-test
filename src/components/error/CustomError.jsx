export function CustomError({statusCode, statusText}){


    return(
        <div>
            <h1 className="text-2xl font-bold">{statusCode} - {statusText} </h1>
        </div>
    )
}