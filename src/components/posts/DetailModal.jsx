"use client";

import { useState } from "react";
import { Modal } from "../public/Modal";

export function Detail(post) {
  const [show, setShow] = useState(false);

  const showModal = () => {
    setShow(true);
  };

  const closeModal = () => {
    setShow(false);
  };

  return (
    <>
      <button className="mr-2" onClick={showModal}>View Details</button>
      {show ? (
        <div>
          <Modal close={closeModal}>
            <div className="flex flex-col">
              <div className="self-end">
                <button className="button-red" onClick={closeModal}>X</button>
              </div>
  
              <h1>{post.title}</h1>
              <p>{post.content}</p>
            </div>
          </Modal>
        </div>
      ) : (
        ""
      )}
    </>
  );
}
