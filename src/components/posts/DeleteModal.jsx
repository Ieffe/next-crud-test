"use client";

import { deletePost } from "@/services/postService";
import { useState } from "react";
import { Modal } from "../public/Modal";
import { useRouter } from "next/navigation";
import useSWR, { mutate } from "swr";

export function DeleteModal(post) {
  const [show, setShow] = useState(false);
  const router = useRouter();

  async function deleteHandler(id) {
    await deletePost(id);
    console.log("deleted!");
    setShow(false);
    router.refresh();
    mutate('/posts')
  }

  const handleClose = () => {
    setShow(false);
  };

  const handleOpen = () => {
    setShow(true);
  };

  return (
    <>
      <button className="button-red" onClick={handleOpen}>
        DELETE
      </button>

      {show ? (
        <Modal close={handleClose}>
          <div className="flex flex-row justify-between">
            <h1 className="font-bold text-2xl mb-2">Confirm Deletion</h1>
            <button onClick={handleClose} className="button-red close-button">X</button>
          </div>

          <div className="mb-2">
            <p>Are you sure to delete this data</p>
            <p>
              Title: <i>{post.title}</i>
            </p>
          </div>

          <div className="flex flex-row justify-end">
            <button className="ml-3" onClick={() => deleteHandler(post._id)}>
              {" "}
              Yes
            </button>
            <button className="ml-3 button-red" onClick={handleClose}>
              No
            </button>
          </div>
        </Modal>
      ) : (
        ""
      )}
    </>
  );
}
