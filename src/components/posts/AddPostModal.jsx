"use client";
import { addPosts } from "@/services/postService";
import { useEffect, useState } from "react";
import { Modal } from "../public/Modal";
import { useRouter } from "next/navigation";

export function AddPost() {
  const router = useRouter();
  const [show, setShow] = useState(false);
  const [form, setForm] = useState({
    title: "",
    content: "",
  });

  const [valid, setValid] = useState({
    title: false,
    content: false,
  });
  const [submitted, setSubmitted] = useState(false);

  const validate = () => {
    const newValid = {
      title: !!form.title,
      content: !!form.content,
    };
    setValid(newValid);
  };

  useEffect(() => {
    if (valid.title && valid.content) {
      handleSubmit();
    }
  }, [valid.title, valid.content]);

  const handleSubmit = async () => {
    const payload = {
      title: form.title,
      content: form.content,
    };

    await addPosts(payload);
    router.refresh();
    setForm({ title: "", content: "" });
    setValid({
      title: false,
      content: false,
    });
    setShow(false);
  };

  const handleOpen = () => {
    setShow(true);
    setSubmitted(false);
  };

  const handleClose = () => {
    setShow(false);
  };

  return (
    <>
      <button className="mb-4 mr-3" onClick={handleOpen}>
        Add New Post
      </button>
      {show ? (
        <>
          <Modal close={handleClose}>
            <div className="flex flex-col">
              <div className="flex justify-between items-center mb-3">
                <h1 className="font-bold text-2xl">Add New Post</h1>
                <button className="close-button" onClick={handleClose}>
                  X
                </button>
              </div>

              <form
                onSubmit={(e) => {
                  e.preventDefault();
                  validate();
                  setSubmitted(true);
                }}
              >
                {!valid.title && submitted && <p>Title must not be empty</p>}
                {/* Display error message on submission */}
                <input
                  onChange={(e) => setForm({ ...form, title: e.target.value })}
                  type="text"
                  placeholder="Insert Title"
                />
                {!valid.content && submitted && (
                  <p>Content must not be empty</p>
                )}{" "}
                {/* Display error message on submission */}
                <textarea
                  onChange={(e) =>
                    setForm({ ...form, content: e.target.value })
                  }
                  name=""
                  placeholder="Add Some Text"
                  id=""
                  cols="30"
                  rows="10"
                ></textarea>
                <button>Submit</button>
              </form>
            </div>
          </Modal>
        </>
      ) : (
        ""
      )}
    </>
  );
}
