"use client";

import { updatePost } from "@/services/postService";
import { useState, useEffect } from "react";
import { Modal } from "../public/Modal";
import { useRouter } from "next/navigation";
import { mutate } from "swr";

export function EditModal(post) {
  const [show, setShow] = useState(false);
  const [form, setForm] = useState({
    title: post.title,
    content: post.content,
  });
  const [valid, setValid] = useState({
    title: false,
    content: false,
  });
  const [submitted, setSubmitted] = useState(false);

  const router = useRouter();
  const validate = () => {
    const newValid = {
      title: !!form.title,
      content: !!form.content,
    };
    setValid(newValid);
  };

  useEffect(() => {
    if (valid.title && valid.content) {
      handleSubmit();
    }
  }, [valid.title, valid.content]);

  const showModal = () => {
    setShow(true);
    setSubmitted(false);
  };

  const closeModal = () => {
    setShow(false);
  };

  const handleSubmit = async () => {
    const payload = JSON.stringify({
      title: form.title,
      content: form.content,
    });

    await updatePost(post._id, payload);
    setShow(false);
    router.refresh();
    mutate('/posts')
  };

  return (
    <>
      <button className="mr-2" onClick={showModal}>
        Edit this Post!
      </button>
      {show ? (
        <Modal close={closeModal}>
          <div className="flex flex-row items-center justify-between">
            <h1 className="font-bold text-2xl mb-3">Edit Post</h1>
            <button className="button-red close-button" onClick={closeModal}>
              X
            </button>
          </div>

          <form
                onSubmit={(e) => {
                  e.preventDefault();
                  validate();
                  setSubmitted(true);
                }}
              >
                {!valid.title && submitted && <p>Title must not be empty</p>}
                {/* Display error message on submission */}
                <input
                  onChange={(e) => setForm({ ...form, title: e.target.value })}
                  type="text"
                  placeholder="Insert Title"
                  value={form.title}
                />
                {!valid.content && submitted && (
                  <p>Content must not be empty</p>
                )}{" "}
                {/* Display error message on submission */}
                <textarea
                  onChange={(e) =>
                    setForm({ ...form, content: e.target.value })
                  }
                  name=""
                  placeholder="Add Some Text"
                  id=""
                  cols="30"
                  rows="10"
                  value={form.content}
                ></textarea>
                <button>Submit</button>
              </form>
        </Modal>
      ) : (
        ""
      )}
    </>
  );
}
