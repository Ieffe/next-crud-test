import { EndPoint, Request } from "@/configs/apiConfigs";
import { notFound, redirect } from "next/navigation";
import { of } from "rxjs";
import useSWR from "swr";

export let apiResp;
export let isLoading = of(false);

let data = []

export async function create(payload) {
    isLoading = of(true);
    console.log("adding new data ...");
  
    let resp = await Request({
      method: "POST",
      url: "/form2",
      payload: payload,
    });
  
    switch (resp.status) {
      case 201:
        data = resp.data;
        isLoading = of(false);
        return true;
      default:
        switch (resp.response.status) {
          case 404:
            console.log(resp.response.status);
            isLoading = of(false);
            notFound();
          default:
            console.log(resp.response.status);
            isLoading = of(false);
            return resp.response;
        }
    }
  }