import { EndPoint, Request } from "@/configs/apiConfigs";
import { notFound, redirect } from "next/navigation";
import { of } from "rxjs";
import useSWR from "swr";

export let apiResp;
export let isLoading = of(false);

export let posts = [];

export const getPostsSWR = () => { //testing for client components fetching
  const fetcher = async (url) => 
    await Request({
      method: "GET",
      url: url,
    });
  
  const { data, error, isLoading } = useSWR(`/posts`, fetcher);
  return { data, error, isLoading };
};

export async function getPosts() {
  
  isLoading = of(true);
  console.log("fetching ...");
  let resp = await Request({
    method: "GET",
    url: "/posts",
  });

  switch (resp.status) {
    case 200:
      posts = resp.data;
      isLoading = of(false);
      return true;
    default:
      switch (resp.response.status) {
        case 404:
          console.log(resp.response.status);
          isLoading = of(false);
          notFound();
        default:
          console.log(resp.response.status);
          isLoading = of(false);
          return resp.response;
      }
  }
}

export async function addPosts(payload) {
  isLoading = of(true);
  console.log("adding new data ...");

  let resp = await Request({
    method: "POST",
    url: "/posts",
    payload: payload,
  });

  switch (resp.status) {
    case 201:
      posts = resp.data;
      isLoading = of(false);
      return true;
    default:
      switch (resp.response.status) {
        case 404:
          console.log(resp.response.status);
          isLoading = of(false);
          notFound();
        default:
          console.log(resp.response.status);
          isLoading = of(false);
          return resp.response;
      }
  }
}

export async function updatePost(id, payload) {
  isLoading = of(true);
  console.log("updating ...");

  let resp = await Request({
    method: "PUT",
    url: `/posts/${id}`,
    payload: payload,
  });

  switch (resp.status) {
    case 200:
      posts = resp.data;
      isLoading = of(false);
      return true;
    default:
      switch (resp.response.status) {
        case 404:
          console.log(resp.response.status);
          isLoading = of(false);
          notFound();
        default:
          console.log(resp.response.status);
          isLoading = of(false);
          return resp.response;
      }
  }
}

export async function deletePost(id) {
  isLoading = of(true);
  let resp = await Request({
    method: "DELETE",
    url: `/posts/${id}`,
  });
  console.log("deleting ...");
  switch (resp.status) {
    case 200:
      posts = resp.data;
      isLoading = of(false);
      return true;
    default:
      switch (resp.response.status) {
        case 404:
          console.log(resp.response.status);
          isLoading = of(false);
          notFound();
        default:
          console.log(resp.response.status);
          isLoading = of(false);
          return resp.response;
      }
  }
}
