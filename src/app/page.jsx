import { CustomError } from "@/components/error/CustomError";
import { AddPost } from "@/components/posts/AddPostModal";
import { DeleteModal } from "@/components/posts/DeleteModal";
import { Detail } from "@/components/posts/DetailModal";
import { EditModal } from "@/components/posts/EditModal";
import { getPosts, posts } from "@/services/postService";

export default async function Home() {
  const api = await getPosts();
  console.log(api)
  return (
    <main className="flex min-h-screen flex-col items-center justify-start p-24">
      {api.status ? (
        <CustomError statusCode={api.status} statusText={api.statusText} />
      ) : (
        <>
          <AddPost />
          <div>
            {posts.length > 0 ? (
              posts.map((post, index) => (
                <div
                  className="mb-4 border border-white p-4 rounded-lg"
                  key={index}
                >
                  <p className="font-bold"> {post.title} </p>
                  <p>{post.content}</p>
                  <div className="mt-2 flex flex-row">
                    <Detail {...post} />
                    <EditModal {...post} />
                    <DeleteModal {...post} />
                  </div>
                </div>
              ))
            ) : (
              <p>There is No Post, try to add one!</p>
            )}
          </div>
        </>
      )}
    </main>
  );
}
