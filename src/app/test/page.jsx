"use client";
import Tabs from "@/configs/tabConfig";
import { FormManagerProvider } from "@/context/FormContext";
import React, { Suspense, useState } from "react";
import Loading from "./loading";
import Nested2 from "@/components/tab/Submenu2";

export default function Page() {
  const [activeTab, setActiveTab] = useState(null);
  const [tabs, setTabs] = useState([]);
  const [submenus, setSubmenus] = useState(Tabs.map(() => false));
  const [isLoading, setIsLoading] = useState(null);

  const addTab = (component, id) => {
    const existingTab = tabs.find((tab) => tab.id === id);

    // setIsLoading(true)

    if (existingTab) {
      setActiveTab(existingTab.component);
    } else {
      const tabToOpen =
        Tabs.find((tab) => tab.id === id) ||
        Tabs.find(
          (tab) =>
            tab.submenus && tab.submenus.some((submenu) => submenu.id === id)
        );

      if (tabToOpen) {
        setTabs((prevTabs) => [...prevTabs, { component, id }]);
        setActiveTab(component);
      }
    }

    // setTimeout(() => {
    //   setIsLoading(false);
    // }, 2000); //uncomment for loading test
  };



  const toggleSubmenu = (index) => {
    //toggle for array of submenus is used
    const updatedSubmenus = [...submenus];
    updatedSubmenus[index] = !updatedSubmenus[index];
    setSubmenus(updatedSubmenus);
  };

  const renderActiveComponent = () => {
    return activeTab || null;
  };

  

  const switchTab = (id) => {
    const selectedTab = tabs.find((tab) => tab.id === id);
    setActiveTab(selectedTab ? selectedTab.component : null);
  };

  const closeTab = (component, index) => {
    const newTabs = tabs.filter((_, id) => id !== index);
    setTabs(newTabs);

    if (activeTab === component) {
      if (index > 0) {
        setActiveTab(newTabs[index - 1].component);
      } else if (newTabs.length > 0) {
        setActiveTab(newTabs[0].component);
      } else {
        setActiveTab(null);
      }
    }
  };

  return (
    <FormManagerProvider>
      <div className="flex flex-row gap-x-5 min-h-screen">
        <div className="flex flex-col w-1/6 pt-10 relative">
          {Tabs.map((tab, index) => (
            <div
              key={index}
              onMouseEnter={(e) => {
                e.stopPropagation();
                toggleSubmenu(index);
              }}
              onMouseLeave={() => toggleSubmenu(index)}
              className="flex flex-row relative justify-center"
            >
              <button
                className="self-center my-2 border-0"
                onClick={() => {
                  if (tab.nested === true) {
                    // Handle nested tab click
                    addTab(<Nested2 tabsList={tab.submenus} />, tab.id);
                  } else {
                    // Handle regular tab click
                    addTab(tab.component, tab.id);
                  }
                }}
              
              >
                {tab.icon ? tab.icon : tab.name}
              </button>
              { !tab.nested && tab.submenus && submenus[index] && (
                <div className="absolute left-[100%] min-w-max">
                  <div className="grid grid-cols-3 bg-black border border-white">
                    {tab.submenus.map((s, subIndex) => (
                      <div
                        key={subIndex}
                        className="flex flex-col items-center p-5"
                        onClick={ () => addTab(s.component, s.id)
                        }
                      >
                        <div className="">{s.icon}</div>
                        <span className="mt-3">{s.name}</span>
                      </div>
                    ))}
                  </div>
                </div>
              )}
            </div>
          ))}
        </div>
        <div className="flex flex-col w-5/6">
          <div className="flex flex-row py-5 justify-end">
            <h1 className="mx-3 font-bold text-2xl">Tabbed App Viewer</h1>
          </div>
          {tabs.length > 0 ? (
            <>
              <div className="flex flex-row">
                {tabs.map((tab, index) => (
                  <div
                    key={index}
                    className={`flex flex-shrink-0  flex-row gap-x-3 justify-between px-3 py-2 border-white rounded-t-lg ${
                      tab.component === activeTab
                        ? "border-x border-t"
                        : " border"
                    }`}
                    onClick={() => switchTab(tab.id)}
                  >
                    <span>
                      {" "}
                      {Tabs.find((t) => t.id === tab.id)?.name ||
                        Tabs.find((t) =>
                          t.submenus?.some((s) => s.id === tab.id)
                        )?.submenus.find((s) => s.id === tab.id)?.name}
                    </span>
                    <span
                      onClick={(e) => {
                        e.stopPropagation();
                        closeTab(tab.component, index);
                      }}
                    >
                      ×
                    </span>
                  </div>
                ))}
                <div className="w-full border-b" />
              </div>
              <div className="content h-full border-l pl-6 pt-6 pb-0">
                {isLoading ? <Loading /> : renderActiveComponent()}
              </div>
            </>
          ) : (
            <p>No active tab.</p>
          )}
        </div>
      </div>
    </FormManagerProvider>
  );
}
