// FormManager.js
import React, { useState, useEffect } from "react";

export const FormManagerContext = React.createContext();

//context is used to retain value in one main component, layout, or page
export const FormManagerProvider = ({ children }) => {
  const [formState, setFormState] = useState({
    title: "",
    content: "",
  });

  const [form2State, setForm2State] = useState({
    a: "",
    b: "",
    c: "",
  });

  const [activeTab, setActiveTab] = useState(null);

  const [step, setStep] = useState(1);

  const updateFormState = (newState) => {
    setFormState((prevState) => ({ ...prevState, ...newState }));
  };

  const updateForm2State = (newState) => {
    setForm2State((prevState) => ({ ...prevState, ...newState }));
  };

  const [tabs, setTabs] = useState([]);
  const [activeN2Tab, setActiveN2Tab] = useState(null);
  const [showSideMenu, setShowSideMenu] = useState(false);

  const [formDirtyData, setFormDirtyData] = useState({
    name: "",
    email: "",
  });

  const [isDirty, setIsDirty] = useState(false);

  useEffect(() => {
    const handleBeforeUnload = (event) => {
      if (isDirty) {
        event.returnValue = undefined; // Standard for most browsers
        return undefined;
      }
    };

    window.addEventListener("beforeunload", handleBeforeUnload);

    return () => {
      window.removeEventListener("beforeunload", handleBeforeUnload);
    };
  }, [isDirty]);

  return (
    <FormManagerContext.Provider
      value={{
        activeTab,
        setActiveTab,
        formState,
        updateFormState,
        form2State,
        updateForm2State,
        step,
        setStep,
        tabs,
        setTabs,
        activeN2Tab,
        setActiveN2Tab,
        showSideMenu,
        setShowSideMenu,
        formDirtyData,
        setFormDirtyData,
        isDirty,
        setIsDirty,
      }}
    >
      {children}
    </FormManagerContext.Provider>
  );
};
